from os import readlink
import pexpect
import sys
from .logging import ExamplePexpectLogger, spawn_extlog_prox

def single_linear_define(params, log, define_str=None, timeout=1, cwd=None):
  """
  Represents a single, linear run through Turbomole's "define"

  Prototypical "naive" implementation without fancy state machines or anything
  like that. Could be made to look much nicer in the future.
  """
  # XXX yes this code looks like garbage, but I don't want to go too crazy with
  # fancy abstractions before I know exactly what types of "dialogues" those
  # abstractions must support => naive huegass function for now
  child = pexpect.spawn("define", echo=False, timeout=timeout, cwd=cwd)
  child = spawn_extlog_prox(child)
  logger = ExamplePexpectLogger(actual=log, line_based_user_input=True)
  child.logger = logger

  # make clear that we don't handle hcore guess here, cf. ASE TM calculator
  assert "hcore" not in params, "hcore should be set via adg/kdg, not define"

  try:
    if define_str is None:
      _define_via_params(child=child, params=params)
    else:
      _define_via_str(child=child, define_str=define_str)

    child.expect("define ended normally")
    child.expect(pexpect.EOF)
  except pexpect.TIMEOUT as e:
    if log in [sys.stdout, sys.stderr]:
      log_path = log.name
    else:
      log_path = readlink(f"/proc/self/fd/{log.fileno()}")
    raise TimeoutException("timeout reached while waiting for response from "\
      f"define - you should check the log under {log_path} for errors and if "\
      "things look OK, try increasing the timeout, which may be too small "\
      "for define's long processing times in case of larger systems") from e

def _define_via_params(child, params):
  p = params # shortcut

  # PRELUDE

  # title
  child.expect("HIT >return<")
  child.send("\n")
  child.expect("INPUT TITLE")
  title = p.get("title", "")
  child.send(f"{title}\n")
  # geometry / coordinates
  child.expect("SPECIFICATION OF MOLECULAR GEOMETRY")
  coord_path = p.get('coord_path', 'coord')
  child.send(f"a {coord_path}\n")
  child.expect(r"CARTESIAN COORDINATES FOR\s+([0-9]+)\s+ATOMS HAVE "\
    "SUCCESSFULLY")
  if p.get("use redundant internals"):
    child.send("ired\n")
    child.send("*\n")
  else:
    child.send("*\n")
    child.expect("IF YOU DO NOT WANT TO USE INTERNAL COORDINATES ENTER  no")
    child.send("no\n")
  # basis set
  child.expect("ATOMIC ATTRIBUTE DEFINITION MENU")
  if "basis set name" in p:
    basis_set = p["basis set name"]
    child.send(f"b all {basis_set}\n")
    r = child.expect([
      "THERE ARE NO DATA SETS CATALOGUED IN FILE.*USE ONE OF THE FOLLOWING "\
        "OPTIONS", # 0
      "ATOMIC ATTRIBUTE DEFINITION MENU" # 1
    ])
    if r == 0:
      raise BasisSetNotFound(f"basis set {basis_set} not found")
  child.send("*\n")
  # occupations, charge, etc.
  child.expect("OCCUPATION NUMBER & MOLECULAR ORBITAL DEFINITION MENU")
  child.send("eht\n")
  child.expect("DO YOU WANT THE DEFAULT PARAMETERS FOR THE EXTENDED HUECKEL "\
    r"CALCULATION \?")
  child.send("y\n")
  # charge
  child.expect("ENTER THE MOLECULAR CHARGE", timeout=5*child.timeout)
  charge = p.get("total charge", 0)
  child.send(f"{charge}\n")
  # occupations / multiplicity
  # underlying goal: if the multiplicity is None, we want to use whatever
  # Turbomole suggested by default. same thing for uhf (where we also need to
  # handle the case in which the multiplicity is given and different from
  # Turbomole's default but uhf isn't => choose rhf for singlets, uhf for
  # everything else)
  found_occ_type = child.expect([
    "FOUND CLOSED SHELL SYSTEM !",                    # 0
    r"FOUND HALF-OPEN SHELL CASE WITH MULTIPLICITY ([0-9]+)\s+", # 1
    r"FOUND MORE THEN HALF-OPEN SHELL CASE WITH MULTIPLICITY ([0-9]+)\s", #2
    r"FOUND LESS THEN HALF-OPEN SHELL CASE WITH MULTIPLICITY ([0-9]+)\s", #3
  ])
  if found_occ_type == 0:
    found_mult = 1
  elif found_occ_type in [1,2,3]:
    found_mult = int(child.match.group(1))
  child.expect(r"DO YOU ACCEPT THIS OCCUPATION \?")
  multiplicity = p.get("multiplicity", None)
  uhf = p.get("uhf", None)
  if (multiplicity is None or found_mult == multiplicity) and (uhf is None
  or (not uhf and found_mult == 1) or (uhf and found_mult != 1)):
    child.send("y\n")
  else:
    child.send("n\n")
    child.expect(r"OCCUPATION NUMBER ASSIGNMENT MENU\s+\(\s*#e=([0-9]+)\s+")
    found_nelect = int(child.match.group(1))
    if multiplicity is None:
      multiplicity = found_mult
    if multiplicity == 1:
      if uhf:
        child.send("s\n")
      else:
        child.send(f"c 1-{found_nelect//2}\n")
    elif multiplicity == 3 and (uhf or uhf is None):
      child.send("t\n")
    else:
      unpaired = multiplicity - 1
      if uhf or uhf is None:
        smear_f = "f" if p.get("use fermi smearing") else ""
        child.send(f"u{smear_f} {unpaired}\n")
      else:
        closed = (found_nelect - unpaired)//2
        if (found_nelect-unpaired)//2 >= 1:
          child.send(f"c 1-{closed}\n")
          child.expect("OCCUPATION NUMBER ASSIGNMENT MENU")
        for i in range(unpaired):
          child.send(f"o {closed+1+i}\n")
          child.expect("INPUT THE OPEN-SHELL OCCUPATION NUMBER PER MO")
          child.send("1\n")
          child.expect(r"THUS THERE ARE \S+ ELECTRONS IN THIS OPEN SHELL\s*\?")
          child.send("\n")
          if i != unpaired-1:
            child.expect("OCCUPATION NUMBER ASSIGNMENT MENU")
    child.expect(r"OCCUPATION NUMBER ASSIGNMENT MENU\s+"\
      r"\(\s*#e=([0-9]+)\s+#(\w)=([0-9]+)\s+#(\w)=([0-9]+)\)")
    # sanity check
    assert ((child.match.group(2) == b"c" and child.match.group(4) == b"o") or
      (child.match.group(2) == b"a" and child.match.group(4) == b"b"))
    if int(child.match.group(3))+int(child.match.group(5)) != found_nelect:
      raise InvalidOccupation("occupations do not add up - are you trying "\
        "to force an illogical multiplicity?")
    child.send("*\n")

  r = None
  while r != 0:
    r = child.expect([
      "GENERAL MENU : SELECT YOUR TOPIC",                          # 0
      r"SPIN STATE IS (\w+), RIGHT\s*\?\s+DEFAULT=y",              # 1
      r"DO YOU REALLY WANT TO WRITE OUT NATURAL ORBITALS \?",      # 2
      r"ROOTHAAN PARAMETERS a AND b COULD NOT BE PROVIDED \.\.\.", # 3
      r"UNDER THESE CIRCUMSTANCES YOU BETTER CONTINUE",            # 4
    ], timeout=30*child.timeout) # this can take forever => wait longer

    if r == 1:
      if multiplicity == 3:
        assert child.match.group(1) == b"TRIPLET"
      else:
        raise NotImplementedError("have to implement check for this")
      child.send("\n")
    elif r == 2:
      child.send("n\n")
    elif r == 3:
      raise NotImplementedError("Turbomole says: 'Roothan parameters could "\
          "not be provided', but I have no idea what to do in this case")
    elif r == 4:
      raise InvalidOccupation("something went wrong when setting up "\
        "occupations - are you trying to force an illogical multiplicity?")

  # MAIN MENU

  # scf options
  child.send("scf\n")
  child.expect("ENTER SCF-OPTION TO BE MODIFIED")
  if p.get("scf iterations") is not None:
    iterations = p["scf iterations"]
    child.send("iter\n")
    child.expect("ENTER NEW VALUE FOR MAXIMUM NUMBER OF SCF-ITERATIONS")
    child.send(f"{iterations}\n")
    child.expect("ENTER SCF-OPTION TO BE MODIFIED")
  if p.get("scf energy convergence") is not None:
    conv = p["scf energy convergence"]
    child.send("conv\n")
    child.expect("ENTER DESIRED ACCURACY OF SCF-ENERGY")
    child.send(f"{conv}\n")
    child.expect("ENTER SCF-OPTION TO BE MODIFIED")
  damp_keys = [ "initial damping", "damping adjustment step",
    "minimal damping" ]
  if any([p.get(x) is not None for x in damp_keys]):
    init_damping, damping_adjust, min_damping = [ p[x] for x in damp_keys ]
    child.send("damp\n")
    child.expect("ENTER START VALUE FOR DAMPING")
    child.send(f"{init_damping}\n")
    child.expect("ENTER INCREMENT FOR REDUCTION OF DAMPING")
    child.send(f"{damping_adjust}\n")
    child.expect("ENTER MINIMAL DAMPING")
    child.send(f"{min_damping}\n")
    child.expect("ENTER SCF-OPTION TO BE MODIFIED")
  if p.get("use fermi smearing"):
    child.send("fermi\n")
    child.expect("Enter number to change or <return> to accept all.")
    if p.get("fermi initial temperature") is not None:
      fermi_initial = p["fermi initial temperature"]
      child.send("1\n")
      child.expect("Enter new initial temperature:")
      child.send(f"{fermi_initial}\n")
      child.expect(f"1\\) initial temperature:\\s+{fermi_initial}")
      child.expect("Enter number to change or <return> to accept all.")
    if p.get("fermi final temperature") is not None:
      fermi_final = p["fermi final temperature"]
      child.send("2\n")
      child.expect("Enter new final temperature:")
      child.send(f"{fermi_final}\n")
      child.expect(f"2\\) final temperature:\\s+{fermi_final}")
      child.expect("Enter number to change or <return> to accept all.")
    if p.get("fermi annealing factor") is not None:
      fermi_anneal = p["fermi annealing factor"]
      child.send("3\n")
      child.expect("Enter new annealing factor:")
      child.send(f"{fermi_anneal}\n")
      child.expect(f"3\\) annealing factor:\\s+{fermi_anneal}")
      child.expect("Enter number to change or <return> to accept all.")
    if p.get("fermi homo-lumo gap criterion") is not None:
      fermi_gap_crit = p["fermi homo-lumo gap criterion"]
      child.send("4\n")
      child.expect("Enter new HOMO-LUMO gap criterion:")
      child.send(f"{fermi_gap_crit}\n")
      child.expect("4\\) HOMO-LUMO gap criterion:\\s+(\\S+)\\s")
      found_fermi_gap_crit = float(child.match.group(1))
      assert abs(found_fermi_gap_crit - fermi_gap_crit)/fermi_gap_crit < 0.1, \
        f"tried to set fermi_gap_crit={fermi_gap_crit}, but Turbomole said "\
        f"it was going to use {found_fermi_gap_crit} instead - what?"
      child.expect("Enter number to change or <return> to accept all.")
    if p.get("fermi stopping criterion") is not None:
      fermi_stop_crit = p["fermi stopping criterion"]
      child.send("5\n")
      child.expect("Enter new stopping criterion:")
      child.send(f"{fermi_stop_crit}\n")
      r = child.expect("5\\) stopping criterion:\\s+(\\S+)\\s")
      found_fermi_stop_crit = float(child.match.group(1))
      assert abs(found_fermi_stop_crit - fermi_stop_crit)/fermi_stop_crit < 0.1, \
        f"tried to set fermi_stop_crit={fermi_stop_crit}, but Turbomole said "\
        f"it was going to use {found_fermi_stop_crit} instead - what?"
      child.expect("Enter number to change or <return> to accept all.")
    child.send("\n")
    child.expect("ENTER SCF-OPTION TO BE MODIFIED")
  child.send("\n")
  child.expect("GENERAL MENU : SELECT YOUR TOPIC")

  # DFT options
  if p.get("use dft"):
    child.send("dft\n")
    child.expect("ENTER DFT-OPTION TO BE MODIFIED")
    child.send("on\n")
    child.expect("ENTER DFT-OPTION TO BE MODIFIED")
    if p.get("density functional") is not None:
      functional = p["density functional"]
      child.send(f"func {functional}\n")
      child.expect(f"functional {functional}")
      child.expect("ENTER DFT-OPTION TO BE MODIFIED")
    if p.get("grid size") is not None:
      grid_size = p["grid size"]
      child.send(f"grid {grid_size}\n")
      child.expect(f"gridsize {grid_size}")
      child.expect("ENTER DFT-OPTION TO BE MODIFIED")
    child.send("*\n")
    child.expect("GENERAL MENU : SELECT YOUR TOPIC")

  # RI options
  if p.get("use resolution of identity"):
    child.send("ri\n")
    child.expect("ENTER RI-OPTION TO BE MODIFIED")
    child.send("on\n")
    child.expect("RI IS USED")
    child.expect("ENTER RI-OPTION TO BE MODIFIED")
    if p.get("ri memory") is not None:
      ri_memory = p["ri memory"]
      child.send(f"m {ri_memory}\n")
      child.expect(f"Memory for RI:\\s+{ri_memory} Mb")
      child.expect("ENTER RI-OPTION TO BE MODIFIED")
    child.send("*\n")
    child.expect("GENERAL MENU : SELECT YOUR TOPIC")

  # TODO setting all the other garbage

  child.send("*\n")

def _define_via_str(child, define_str):
  child.send(define_str)
  if len(define_str) < 1 or define_str[-1] != "\n":
    child.send("\n")
  child.sendeof()

class BasisSetNotFound(Exception):
  pass

class InvalidOccupation(Exception):
  pass

class TimeoutException(Exception):
  pass

from abc import ABCMeta, abstractmethod
import pexpect

class AbstractPexpectLogger(metaclass=ABCMeta):
  @abstractmethod
  def on_expect(self, spawn, *args, **kwargs): pass

  @abstractmethod
  def on_send(self, spawn, s): pass

class AbstractWritingPexpectLogger(AbstractPexpectLogger):
  @abstractmethod
  def write(self, content): pass

class AbstractCategorizingPexpectLogger(AbstractWritingPexpectLogger):
  def on_expect(self, spawn, *args, **kwargs):
    if spawn.before is not None:
      self.write_before(spawn.before)

    if spawn.match is not None and spawn.match is not pexpect.EOF:
      self.write_match(spawn.match[0])

    if spawn.after is not None and spawn.after is not pexpect.EOF:
      actual_after = spawn.after[len(spawn.match[0]):]
      if len(actual_after) > 0:
        self.write_after(actual_after)

  def on_send(self, spawn, s):
    self.write_input(spawn._coerce_send_string(s))

  # XXX not sure if this is needed
  def on_read(self, s):
    self.write_after(s)

  def on_timeout(self, spawn, *args, **kwargs):
    self.write_before(spawn.before)

  def on_eof(self, spawn, *args, **kwargs):
    self.write_before(spawn.before)

  @abstractmethod
  def write_before(self, content): pass

  @abstractmethod
  def write_match(self, content): pass

  @abstractmethod
  def write_after(self, content): pass

  @abstractmethod
  def write_input(self, content): pass

class PrefixingPexpectLogger(AbstractCategorizingPexpectLogger):
  def __init__(self, actual, line_based_user_input=False):
    self._actual = actual
    self._line_based_user_input = line_based_user_input
    self._already_wrote_something = False

  def write_before(self, content):
    for line in content.splitlines():
      self.write(b" | ")
      self.write(line)
      self.write(b"\n")

  def write_match(self, content):
    for line in content.splitlines():
      self.write(b"*| ")
      self.write(line)
      self.write(b"\n")

  def write_after(self, content):
    for line in content.splitlines():
      self.write(b" | ")
      self.write(line)
      self.write(b"\n")

  def write_input(self, content):
    for line in content.splitlines():
      self.write(b">| ")
      self.write(line)
      self.write(b"\n")

  def write(self, content):
    self._actual.write(content)

class LineCountingMixin:
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self._lines_written = 0
    self.match_at_line = None

  def write_match(self, content):
    self.match_at_line = self._lines_written + 1
    super().write_match(content)

  def write(self, content):
    super().write(content)
    self._lines_written += content.count(b"\n")


class ExamplePexpectLogger(LineCountingMixin, PrefixingPexpectLogger):
  pass


import wrapt
class spawn_extlog_prox(wrapt.ObjectProxy):
  logger = None

  def expect(self, *args, **kwargs):
    try:
      ret = self.__wrapped__.expect(*args, **kwargs)
      if self.logger is not None:
        self.logger.on_expect(self, *args, **kwargs)
    except pexpect.exceptions.TIMEOUT as e:
      if self.logger is not None:
        self.logger.on_timeout(self, *args, **kwargs)
      raise e
    except pexpect.exceptions.EOF as e:
      if self.logger is not None:
        self.logger.on_eof(self, *args, **kwargs)
      raise e
    return ret

  # XXX not sure if this is needed
  def read(self, *args, **kwargs):
    ret = self.__wrapped__.read(*args, **kwargs)
    if self.logger is not None:
      self.logger.on_read(ret)
    return ret

  def send(self, s):
    ret = self.__wrapped__.send(s)
    if self.logger is not None:
      self.logger.on_send(self, s)
    return ret

def spawn_extlog(*args, **kwargs):
  return spawn_extlog_prox(pexpect.spawn(*args, **kwargs))

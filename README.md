
# reasonablypacedmole

[![coverage report](https://smheidrich.gitlab.io/reasonablypacedmole/htmlcov/coverage.svg)](https://smheidrich.gitlab.io/reasonablypacedmole/htmlcov/)

Sane Python interface for Turbomole's define

## Motivation

Most scripts that interface with Turbomole's interactive ``define`` program for
setting up quantum chemistry calculations blindly feed a string of commands
into it, with the author crossing their fingers that everything will run
smoothly or fail in obvious ways. When they don't, one can end up wasting a lot
of time trying to diagnose the problem, especially since "something went wrong
during calculation definition" is usually only one of dozens of possibilities
for where the issue originated.

This library aims to improve the situation by using
[pexpect](https://pexpect.readthedocs.io/) to actually "talk" to ``define``,
ensuring it affirms that the various options were understood and raising
exceptions if they weren't.

## Status & roadmap

Right now, the immediate goal is to achieve feature parity with the "define
string" constructed inside
[ASE's Turbomole calculator](https://wiki.fysik.dtu.dk/ase/ase/calculators/turbomole.html)
so that it can (hopefully) be integrated into ASE in the string's stead.
At that point, there will also be a solid basis of automated tests so that
further improvements should be easier to code.
I'm about 90% of the way there.

ASE only supports a small subset of Turbomole's settings, however, so the next
step will be to generalize the library so that all of these become accessible.
Ideally this should also make it possible to interact with parts of ``define``
that weren't explicitly hardcoded into the library (e.g. after a new Turbomole
version is released) via some heuristics, although I'm not yet 100% sure on
that.

## Installation

```bash
pip install https://gitlab.com/smheidrich/reasonablypacedmole.git
```

## Usage

In a directory that already has a `coord` file with coordinates:

```python
from reasonablypacedmole import single_linear_define
params = { "use dft": True, "density functional": "b3-lyp", }
with open("define.log", "wb") as f:
  single_linear_define(params, log=f, timeout=5)
```

`params` supports any of the
[ASE calculator's parameters](https://wiki.fysik.dtu.dk/ase/ase/calculators/turbomole.html#parameters),
with the following changes:

- `scf energy convergence` takes the absolute value of the exponent instead of
  actual value (as Turbomole itself does)
- defaults are 100% Turbomole's own defaults, we don't try to set our own:
  - DFT is turned off by default
  - multiplicity can be left unspecified, in which case whatever Turbomole
    itself chose will be used
  - effective default values for `uhf` based on `multiplicity` may be different
    from what ASE's calculator does in edge cases

`timeout` specifies how long `pexpect` will wait for a response from `define`
before giving up and raising an exception. For very large systems, it might
be necessary to increase this to larger values, as `define` itself becomes
slow.

An example `define.log` output file can be found
[here](https://gitlab.com/smheidrich/reasonablypacedmole/-/snippets/2019133).
Inputs, outputs, and matches are annotated in the leftmost column with `>`, `
`, and `*`, respectively, to make diagnosing problems easier, should they
occur.

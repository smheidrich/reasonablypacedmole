from reasonablypacedmole import single_linear_define, BasisSetNotFound, \
  InvalidOccupation
from functools import partial
import os
import pytest
import re
from shutil import which

has_turbomole = which("dscf") is not None and which("jobex") is not None
pytestmark = pytest.mark.skipif(not has_turbomole, reason="requires Turbomole")

# shorthand (saves a line per test)
def single_linear_define_with_default_log(*args, **kwargs):
  with open("define.log", "wb") as f:
    single_linear_define(*args, **kwargs, log=f)

# another (bit more hacky) shorthand
class file_re:
  def __init__(self, path):
    self.path = path
  def __getattr__(self, attr):
    with open(self.path) as f:
      return partial(getattr(re, attr), string=f.read())
  def find_one(self, *args, **kwargs):
    matches = list(self.finditer(*args, **kwargs))
    assert len(matches) > 0, "found no matches for "\
      f"{args=}, {kwargs=}"
    assert len(matches) < 2, "found more than one match for "\
      f"{args=}, {kwargs=}"
    return matches[0]

h2_coords_contents = """\
$coord
    0.00000000000000      0.00000000000000      0.69652092463935      h
    0.00000000000000      0.00000000000000     -0.69652092463935      h
$user-defined bonds
$end
"""

@pytest.fixture
def tmpdir_cwd_with_h2_coord(tmpdir):
  os.chdir(str(tmpdir))
  with open("coord", "w") as f:
    f.write(h2_coords_contents)

def test_h2_title(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "title": "H2test",
  })
  assert file_re("control").find_one(r"\$title\s+H2test\s+")

def test_h2_empty_except_coords(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
  })
  # some random checks: DFT off, def-SV(P) chosen as default basis
  control_re = file_re("control")
  assert control_re.search("dft") is None, "DFT should be off by default"
  assert control_re.find_one(r'\s*basis =[^\n]*def-SV\(P\)')

def test_h2_ired(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "use redundant internals": True,
  })

def test_h2_explicit_basis_set(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "basis set name": "def2-TZVP",
  })
  assert file_re("control").find_one(r'\s*basis =[^\n]*def2-TZVP')

def test_h2_explicit_multiplicity(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "multiplicity": 1,
  })
  assert file_re("control").find_one(r'\$closed shells\n a[ ]+1\s')

def test_h2_charged(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "total charge": -1,
    "multiplicity": 2,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-2\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1\s')

def test_h2_uhf(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "uhf": True,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1\s')

def test_h2_uhf_explicit_mult(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "multiplicity": 1,
    "uhf": True,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1\s')

def test_h2_triplet(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "multiplicity": 3,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-2\s')
  assert file_re("control").search(r'\$beta shells\n a[ ]+0\s') is None

def test_h2_triplet_rohf(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "multiplicity": 3,
    "uhf": False,
  })
  assert file_re("control").find_one(r'\$open shells type=1\n a[ ]+1-2\s')

def test_h2_uncharged_doublet_that_cant_work(tmpdir_cwd_with_h2_coord):
  with pytest.raises(InvalidOccupation):
    single_linear_define_with_default_log({
      "multiplicity": 2,
    })

def test_h2_damp(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "initial damping": 1.0,
    "damping adjustment step": 2.0,
    "minimal damping": 3.0,
  })
  match = file_re("control").find_one(r'\$scfdamp[ ]+start=[ ]*([0-9.]+)[ ]+step=[ ]*([0-9.]+)[ ]+min=[ ]*([0-9.]+)')
  assert float(match.group(1)) == pytest.approx(1.0)
  assert float(match.group(2)) == pytest.approx(2.0)
  assert float(match.group(3)) == pytest.approx(3.0)

def test_h2_scfiter(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "scf iterations": 300,
  })
  with open("control") as f:
    for line in f:
      if line.startswith("$scfiterlimit"):
        if line.split()[1] == "300":
          return
    assert False, "no scfiterlimit found in control file!"

def test_h2_scfconv(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "scf energy convergence": 5,
  })
  with open("control") as f:
    for line in f:
      if line.startswith("$scfconv"):
        if line.split()[1] == "5":
          return
    assert False, "no scfconv found in control file!"

def test_h2_fermi(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "use fermi smearing": True,
    "fermi initial temperature": 400,
    "fermi final temperature": 200,
    "fermi annealing factor": 0.9,
    "fermi homo-lumo gap criterion": 0.08,
    "fermi stopping criterion": 0.004,
  })
  # $fermi tmstrt=400.00 tmend=200.00 tmfac=0.900 hlcrt=8.0E-02 stop=4.0E-03
  with open("control") as f:
    for line in f:
      if line.startswith("$fermi"):
        d = {}
        for assignment in line.split()[1:]:
          lhs, rhs = assignment.split("=")
          d[lhs] = float(rhs)
    assert d["tmstrt"] == pytest.approx(400)
    assert d["tmend"] == pytest.approx(200)
    assert d["tmfac"] == 0.9
    assert d["hlcrt"] == 0.08
    assert d["stop"] == 0.004

def test_h2_dft(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "use dft": True,
    "density functional": "b3-lyp",
  })
  with open("control") as f:
    found_dft_block, found_functional = False, False
    for line in f:
      if line.startswith("$dft"):
        found_dft_block = True
        continue
      if line.strip().startswith("functional"):
        assert line.split()[1] == "b3-lyp"
        found_functional = True
        break
    assert found_dft_block
    assert found_functional

def test_h2_dft_ri(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "use dft": True,
    "density functional": "b3-lyp",
    "use resolution of identity": True,
  })
  assert file_re("control").find_one(r'\$dft\n')
  assert file_re("control").find_one(r'[ ]+functional[ ]+b3-lyp\n')
  assert file_re("control").find_one(r'\$ricore')

def test_h2_dft_grid_size(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "use dft": True,
    "grid size": "m4",
  })
  assert file_re("control").find_one(r'\$dft\n')
  assert file_re("control").find_one(r'[ ]+gridsize[ ]+m4\n')

def test_h2_dft_ri_mem(tmpdir_cwd_with_h2_coord):
  single_linear_define_with_default_log({
    "use dft": True,
    "density functional": "b3-lyp",
    "use resolution of identity": True,
    "ri memory": 1000,
  })
  assert file_re("control").find_one(r'\$dft\n')
  assert file_re("control").find_one(r'[ ]+functional[ ]+b3-lyp\n')
  assert file_re("control").find_one(r'\$ricore[ ]+1000\n')

def test_h2_fails_with_invalid_basis_set(tmpdir_cwd_with_h2_coord):
  with pytest.raises(BasisSetNotFound):
    single_linear_define_with_default_log({
      "basis set name": "defgarbage-SVgarbage",
    })

def test_h2_define_str(tmpdir_cwd_with_h2_coord):
  define_str = '\n\na coord\n*\nno\n*\neht\n\n\n\n*\n'
  single_linear_define_with_default_log({}, define_str=define_str)

@pytest.mark.xfail(reason="can't have warning here or ASE tests freak out")
def test_h2_define_str_warn_if_nonempty_params(tmpdir_cwd_with_h2_coord):
  define_str = '\n\na coord\n*\nno\n*\neht\n\n\n\n*\n'
  with pytest.warns(None):
    single_linear_define_with_default_log({ "use dft": True },
      define_str=define_str)

def test_h2_define_str_without_terminating_newline(tmpdir_cwd_with_h2_coord):
  define_str = '\n\na coord\n*\nno\n*\neht\n\n\n\n*'
  single_linear_define_with_default_log({}, define_str=define_str)

au13_coords_contents = """\
$coord
    4.63762204312598      4.63762204312598      4.63762204312598      au
    9.27524408625195      4.63762204312598      1.77141399349839      au
    9.27524408625195      4.63762204312598      7.50383009275356      au
    0.00000000000000      4.63762204312598      1.77141399349839      au
    0.00000000000000      4.63762204312598      7.50383009275356      au
    1.77141399349839      9.27524408625195      4.63762204312598      au
    7.50383009275356      9.27524408625195      4.63762204312598      au
    1.77141399349839      0.00000000000000      4.63762204312598      au
    7.50383009275356      0.00000000000000      4.63762204312598      au
    4.63762204312598      1.77141399349839      9.27524408625195      au
    4.63762204312598      7.50383009275356      9.27524408625195      au
    4.63762204312598      1.77141399349839      0.00000000000000      au
    4.63762204312598      7.50383009275356      0.00000000000000      au
$end
"""
@pytest.fixture
def tmpdir_cwd_with_au13_coord(tmpdir):
  os.chdir(str(tmpdir))
  with open("coord", "w") as f:
    f.write(au13_coords_contents)

def test_au13_mult_unspecified(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
  })
  # default mult is 6
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-126\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-121\s')

def test_au13_mult6_same_as_default(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "multiplicity": 6,
  })
  # just check that it's unchanged
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-126\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-121\s')

def test_au13_mult4(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "multiplicity": 4,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-125\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-122\s')

def test_au13_chargedm1_mult5_same_as_default(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": -1,
    "multiplicity": 5,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-126\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-122\s')

def test_au13_chargedm1_mult1_rohf(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": -1,
    "multiplicity": 1,
    "uhf": False,
  })
  assert file_re("control").find_one(r'\$closed shells\n a[ ]+1-124\s')

def test_au13_chargedm1_mult3(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": -1,
    "multiplicity": 3,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-125\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-123\s')

def test_au13_chargedm1_mult7(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": -1,
    "multiplicity": 7,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-127\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-121\s')

@pytest.mark.xfail(reason="roothan parameter thing - don't know what to do")
def test_au13_chargedm1_mult7_rohf(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": -1,
    "multiplicity": 7,
    "uhf": False,
  })

def test_au13_chargedp1_mult5_same_as_default(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": 1,
    "multiplicity": 5,
  })
  assert file_re("control").find_one(r'\$alpha shells\n a[ ]+1-125\s')
  assert file_re("control").find_one(r'\$beta shells\n a[ ]+1-121\s')

@pytest.mark.xfail(reason="roothan parameter thing - don't know what to do")
def test_au13_chargedp1_mult5_rohf(tmpdir_cwd_with_au13_coord):
  single_linear_define_with_default_log({
    "total charge": 1,
    "multiplicity": 5,
    "uhf": False,
  })
